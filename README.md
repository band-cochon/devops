# DevOps for Band Cochon

Collection of Ansible playbook to manage Band Cochon

* `deploy-bandcochon-core`: Deploy bandcochon onto the remote server using GIT
* `bandcochon-db-backup`: Backup database
* `bandcochon-image-backup`: Backup image 

